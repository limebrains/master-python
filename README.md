# master-python

Repository with materials for learning back-end developement - Python, Flask, Django

## Basic Python

[General intro to Python](http://www.pythonforbeginners.com/basics/python-quick-guide)

[Google Python crashcourse (5h)](https://www.youtube.com/watch?v=tKTZoB2Vjuk&feature=channel)

[Learn Python3 - Python basics + Idiomatic Python](https://github.com/jerry-git/learn-python3/blob/master/README.md)

[Visualize Python](http://pythontutor.com/visualize.html#mode=edit )

## Advanced Python

[(Documentation)](http://www.tutorialspoint.com/python/)

[Decorators](https://realpython.com/blog/python/primer-on-python-decorators/)

[Generators](https://realpython.com/blog/python/introduction-to-python-generators/)

[Inner functions](https://realpython.com/blog/python/inner-functions-what-are-they-good-for/)

[String formating in python](https://pyformat.info/)

[Type annotations](https://www.caktusgroup.com/blog/2017/02/22/python-type-annotations/)

[Packaging](http://www.pythonforbeginners.com/basics/how-to-use-pip-and-pypi)

[Virtualenv](https://packaging.python.org/guides/installing-using-pip-and-virtualenv/)

[py.test](http://pythontesting.net/framework/pytest/pytest-introduction/)

[pep8 - Python style-guide](https://www.python.org/dev/peps/pep-0008/)

[pep8 styleguide checker](https://pypi.python.org/pypi/pep8)

[CheckiO - programming challenges for mastering Python](https://py.checkio.org)

## More Python

[Python "must watch"](https://github.com/s16h/py-must-watch)

[Python podcast](https://talkpython.fm/)

[Python subreddit](http://www.reddit.com/r/python)

## Flask
[(Documentation)](https://flask.palletsprojects.com/en/1.1.x/)

[Python + Flask course](https://www.udemy.com/course/python-and-flask-bootcamp-create-websites-using-flask/)

## Celery
[(Documentation)](http://docs.celeryproject.org/en/v4.2.2/getting-started/first-steps-with-celery.html)

[Introduction](https://www.slideshare.net/mahendram/introduction-to-python-celery)

[How Celery works](https://www.vinta.com.br/blog/2017/celery-overview-archtecture-and-how-it-works/)

[Getting started](https://www.agiliq.com/blog/2015/07/getting-started-with-celery-and-redis/)

[Sample project](https://github.com/PythonicNinja/test_celery)

## Django
[(Documentation)](https://docs.djangoproject.com/en/2.2/topics/)

[Start with Django](https://www.djangoproject.com/start/)

[Try Django 2.2 - Compact Django tutorial](https://www.youtube.com/watch?v=-oQvMHpKkms)

[Advanced Django course](https://www.udemy.com/course/django-python-advanced/)

[DjangoLearing subreddit - good place for asking questions and looking for answers](https://www.reddit.com/r/djangolearning/)

#### Useful django apps

Django allows instalation of extensions to increase it's functionality even more. Below are some we find especially appealing:

[django-rest-framework](http://www.django-rest-framework.org/) - One of the most popular and powerful Django toolkits. Implements advanced API views, 
additional authorization methods, and more.

[django-extensions](https://github.com/django-extensions/django-extensions) - Adds few useful tools to Django arsenal, such as `shell-plus`, an improved Django shell.

[django-braces](https://django-braces.readthedocs.io/en/latest/) - Mixins for Django class views

[django-debug-toolbar](https://pypi.org/project/django-debug-toolbar/) - Configurable toolbar providing multiple debugging tools.

[cookiecutter-django](https://github.com/pydanny/cookiecutter-django) - Django project jumpstarter. Install everything you need from one tool.

[django-rest-framework-social-oauth2](https://github.com/PhilipGarnero/django-rest-framework-social-oauth2) - As the name suggests, implements social authorization to DRF

[dev-fixtures](https://django-devfixtures.readthedocs.io/en/latest/readme.html#installation) - Tool for easy migration of fixtures


